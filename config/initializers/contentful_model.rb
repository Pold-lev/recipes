ContentfulModel.configure do |config|
  config.access_token = ENV.fetch('CONTENTFUL_TOKEN') # Required
  config.space = ENV.fetch('CONTENTFUL_SPACE') # Required
  config.environment = ENV.fetch('CONTENTFUL_ENV') # Optional - defaults to 'master'
  config.options = { # Optional
                     # Extra options to send to the Contentful::Client and Contentful::Management::Client
                     # See https://github.com/contentful/contentful.rb#configuration

                     # Optional:
                     # Use `delivery_api` and `management_api` keys to limit to what API the settings
                     # will apply. Useful because Delivery API is usually visitor facing, while Management
                     # is used in background tasks that can run much longer. For example:
                     log_level: ::Logger::DEBUG,
                     delivery_api: {
                       timeout_write: 3,
                       timeout_read: 3,
                       timeout_connect:2,
                       log_level: ::Logger::DEBUG
                     },
                     management_api: {
                       timeout_write: 10,
                       timeout_read: 10,
                       timeout_connect:2
                     }
  }
end
