## Marley Spoon web challenge
Details: https://gist.github.com/lawitschka/063f2e28bd6993cac5f8b40b991ae899
## How-tos
#### Setup
For running locally copy Cloudinary credentials into .env file.
#### Run application
`docker-compose up web`  
http://0.0.0.0:3000/
#### Test
`docker-compose run --rm dev rspec spec`  
Coverage 100%.  
Report could be found in the *coverage* folder.
#### Lint
`docker-compose run --rm dev rubocop`
#### Teardown
`docker-compose down -v`
## Main concerns
### Cloudinary availability
If Cloudinary responds too slow waiting for it might slowdown the whole app.  

**Solution:** To address that Cloudinary client timeouts set reasonably low to act as a circuit breaker.

### Size of recipe set and pagination
Loading and rendering whole list of recipes at once is bad for UX and performance.  

**Solution:** Cloudinary client provides api for pagination. For recipes default page size is set to 20.

### Performance and over-fetching of recipes fields
Recipe list is not required to have all the recipe fields or high resolution image.  

**Solution:** Subset of fields for index selected via Cloudinary client. Image resolution params embedded into image url and adjusted in presenters.
### Possible improvements left out of scope
- **Recipe view is missing pagination controls.**  
  Pagination implemented only on the back end as a safety feature.
- **Localisation.**  
  It also seems to be missing in Contentful.
- **Caching.**  
  Could be implemented in many ways. Not necessarily in the application.
- **Error handling.**  
  If Contentful is unavailable there is nothing application or user can do about that.  
  Caching can help to mitigate short outages.  
  UX could be improved on the front end with a better 500 error page.
- **Linter.**  
  Rubocop gets triggered by few rails generated files.  
