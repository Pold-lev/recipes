RSpec.configure do |config|
  config.around do |example|
    cassette = example.metadata[:cassette]
    if cassette
      VCR.use_cassette(
        cassette,
        record: :once,
        allow_playback_repeats: true
      ) { example.run }
    else
      example.run
    end
  end
end

VCR.configure do |config|
  config.cassette_library_dir =
    File.expand_path('../fixtures/vcr_cassettes', __dir__)
  config.hook_into :webmock
  config.before_record do |interaction|
    interaction.response.body.force_encoding('UTF-8')
  end

  config.default_cassette_options = { decode_compressed_response: true }

  config.filter_sensitive_data('{CONTENTFUL_SPACE}') do
    ENV.fetch('CONTENTFUL_SPACE')
  end

  config.filter_sensitive_data('{CONTENTFUL_TOKEN}') do
    ENV.fetch('CONTENTFUL_TOKEN')
  end
end
