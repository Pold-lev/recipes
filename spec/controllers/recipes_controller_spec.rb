require 'rails_helper'

describe RecipesController, cassette: 'recipes' do
  render_views

  describe '#index' do
    let(:recipe_titles) do
      [
        'Crispy Chicken and Rice	with Peas &amp; Arugula Salad',
        'Grilled Steak &amp; Vegetables with Cilantro-Jalapeño Dressing',
        'Tofu Saag Paneer with Buttery Toasted Pita',
        'White Cheddar Grilled Cheese with Cherry Preserves &amp; Basil'
      ]
    end

    context 'when recipes present' do
      before { get :index }

      it 'decorates recipes' do
        expect(assigns(:tiles))
          .to all(be_a_kind_of(Content::RecipeAsTilePresenter))
      end

      it 'calculates total number of pages' do
        expect(assigns(:pages_total)).to eq(1)
      end

      it 'responds with OK' do
        expect(response).to have_http_status(:ok)
      end

      it 'renders index' do
        expect(response).to render_template(:index)
      end

      it 'renders four recipes' do
        expect(response.body)
          .to include('<div class="card-body">').exactly(4).times
      end

      it 'renders recipe titles' do
        expect(response.body).to include(*recipe_titles)
      end
    end

    context 'when paginated' do
      before { stub_const('RecipesController::DEFAULT_PAGE_SIZE', 2) }

      it 'calculates total number of pages' do
        get :index, params: { page: 1 }

        expect(assigns(:pages_total)).to eq(2)
      end

      it 'renders first half of recipes' do
        get :index, params: { page: 1 }

        expect(response.body).to(
          include(*recipe_titles[0..1]) &&
            include('<div class="card-body">').exactly(2).times
        )
      end

      it 'renders second half of recipes' do
        get :index, params: { page: 2 }

        expect(response.body).to(
          include(*recipe_titles[2..3]) &&
            include('<div class="card-body">').exactly(2).times
        )
      end
    end

    context 'when no recipes' do
      before { get :index, params: { page: 2 } }

      it 'finds recipes' do
        expect(assigns(:tiles)).to eq []
      end

      it 'responds with OK' do
        expect(response).to have_http_status(:ok)
      end

      it 'renders index' do
        expect(response).to render_template(:index)
      end

      it 'renders no recipes' do
        expect(response.body).not_to include('<div class="card-body">')
      end
    end
  end

  describe '#show' do
    before { get :show, params: { id: recipe_id } }

    context 'when recipe not found' do
      let(:recipe_id) { 'qwerty123' }

      it 'responds with not found' do
        expect(response).to have_http_status(:not_found)
      end

      it 'renders default 404 page' do
        expect(response.body)
          .to include("The page you were looking for doesn't exist")
      end
    end

    context 'when recipe found' do
      let(:recipe_id) { '437eO3ORCME46i02SeCW46' }

      it 'decorates recipe' do
        expect(assigns(:recipe_details))
          .to be_a_kind_of(Content::RecipeDetailsPresenter)
      end

      it 'responds with OK' do
        expect(response).to have_http_status(:ok)
      end

      it 'renders show' do
        expect(response).to render_template(:show)
      end

      it 'renders recipe title' do
        expect(response.body)
          .to include('Crispy Chicken and Rice	with Peas &amp; Arugula Salad')
      end

      it 'renders recipe description' do
        expect(response.body).to(
          include('Crispy chicken skin, tender meat, and rich, tomatoey sauce')
        )
      end

      it 'renders recipe chef name' do
        expect(response.body).to(include('Jony Chives'))
      end

      it 'renders recipe tags' do
        expect(response.body).to include('gluten free', 'healthy')
      end
    end
  end
end
