require 'rails_helper'

describe ApplicationHelper do
  describe '#markdown' do
    let(:raw) do
      '*Grilled Cheese 101*: Use __mayonnaise__ on the outside'\
      ' of the bread to achieve the ultimate, crispy, '\
      'golden-brown __grilled cheese__. Cook, relax, and enjoy!'
    end

    it 'generates html' do
      expect(helper.markdown(raw)).to(
        eq(
          '<p><em>Grilled Cheese 101</em>: Use <strong>mayonnaise</strong> on'\
          ' the outside of the bread to achieve the ultimate, crispy, '\
          "golden-brown <strong>grilled cheese</strong>. Cook, relax, and enjoy!</p>\n"
        )
      )
    end

    it 'generates html safe' do
      expect(helper.markdown(raw)).to be_html_safe
    end
  end
end
