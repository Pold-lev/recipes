require 'rails_helper'

describe Content::RecipeDetailsPresenter, cassette: 'recipes' do
  subject(:presenter) { described_class.new(recipe) }

  let(:recipe) { Content::Recipe.find('437eO3ORCME46i02SeCW46') }

  describe '#title' do
    it { expect(presenter.title).to eq recipe.title }
  end

  describe '#image_url' do
    it do
      expect(presenter.image_url).to(
        eq('//images.ctfassets.net/kk2bw5ojx476/5mFyTozvSoyE0Mqseoos86/fb88f4302cfd184492e548cde11a2555'\
             '/SKU1479_Hero_077-71d8a07ff8e79abcb0e6c0ebf0f3b69c.jpg')
      )
    end
  end

  describe '#chef_name' do
    context 'when chef exists' do
      it { expect(presenter.chef_name).to eq 'Jony Chives' }
    end

    context 'when no chef' do
      before { allow(recipe).to receive(:chef).and_return(nil) }

      it { expect(presenter.chef_name).to be_nil }
    end
  end

  describe '#tags' do
    it { expect(presenter.tags).to contain_exactly('gluten free', 'healthy') }
  end

  describe '#description' do
    it do
      expect(presenter.description)
        .to be_a(String) && be_present && eq(recipe.description)
    end
  end
end
