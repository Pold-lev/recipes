require 'rails_helper'

describe Content::RecipeAsTilePresenter, cassette: 'recipes' do
  subject(:presenter) { described_class.new(recipe) }

  let(:recipe) { Content::Recipe.find('437eO3ORCME46i02SeCW46') }

  describe '#title' do
    it { expect(presenter.title).to eq recipe.title }
  end

  describe '#image_url' do
    it do
      expect(presenter.image_url).to(
        eq('//images.ctfassets.net/kk2bw5ojx476/5mFyTozvSoyE0Mqseoos86/fb88f4302cfd184492e548cde11a2555'\
             '/SKU1479_Hero_077-71d8a07ff8e79abcb0e6c0ebf0f3b69c.jpg?w=600')
      )
    end
  end

  describe '#details_url' do
    it { expect(presenter.details_url).to eq '/recipes/437eO3ORCME46i02SeCW46' }
  end
end
