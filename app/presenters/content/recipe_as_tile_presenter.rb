# frozen_string_literal: true

module Content
  class RecipeAsTilePresenter
    include Rails.application.routes.url_helpers
    delegate :title, to: :recipe

    def initialize(recipe)
      @recipe = recipe
    end

    def image_url
      recipe.photo.resize(600).load
    end

    def details_url
      recipe_path(recipe.id)
    end

    private

    attr_reader :recipe
  end
end
