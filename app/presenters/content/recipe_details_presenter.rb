# frozen_string_literal: true

module Content
  class RecipeDetailsPresenter < Content::RecipeAsTilePresenter
    delegate :description, to: :recipe

    def image_url
      recipe.photo.load
    end

    def chef_name
      recipe.chef&.name
    end

    def tags
      recipe.tags.map(&:name)
    end
  end
end
