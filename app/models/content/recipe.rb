# frozen_string_literal: true

module Content
  class Recipe < ContentfulModel::Base
    self.content_type_id = 'recipe'
    has_one :chef, class_name: 'Content::Chef'
    has_many :tags, class_name: 'Content::Tag'
  end
end
