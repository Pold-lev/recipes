# frozen_string_literal: true

module Content
  class Tag < ContentfulModel::Base
    self.content_type_id = 'tag'
  end
end
