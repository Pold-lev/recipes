# frozen_string_literal: true

module Content
  class Chef < ContentfulModel::Base
    self.content_type_id = 'chef'
  end
end
