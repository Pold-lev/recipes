# frozen_string_literal: true

module ApplicationHelper
  def markdown(raw)
    Markdown.new(
      raw,
      :hard_wrap, :autolink, :no_intra_emphasis, :fenced_code_blocks
    ).to_html.html_safe
  end
end
