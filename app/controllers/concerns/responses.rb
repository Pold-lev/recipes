# frozen_string_literal: true

module Responses
  extend ActiveSupport::Concern

  def not_found
    render file: Rails.root.join('public/404.html'),
           layout: false,
           status: :not_found
  end
end
