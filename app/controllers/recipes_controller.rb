# frozen_string_literal: true

class RecipesController < ApplicationController
  DEFAULT_PAGE_SIZE = 20
  DEFAULT_PAGE_ORDERING = 'sys.updatedAt'

  def index
    recipes = load_recipes
    @tiles = recipes.map { |recipe| Content::RecipeAsTilePresenter.new(recipe) }
    @pages_total = page_count(recipes.total)
  end

  def show
    not_found if recipe.blank?

    @recipe_details = Content::RecipeDetailsPresenter.new(recipe)
  end

  private

  def load_recipes
    Content::Recipe.paginate(
      params.permit(:page).fetch('page', 1).to_i,
      DEFAULT_PAGE_SIZE,
      DEFAULT_PAGE_ORDERING,
      { select: %w[fields.title fields.photo] }
    ).load
  end

  def recipe
    Content::Recipe.find(params.require(:id))
  end

  def page_count(total)
    (total.to_f / DEFAULT_PAGE_SIZE).ceil
  end
end
