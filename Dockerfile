FROM ruby:2.7.2
RUN apt-get update -qq \
  && apt-get install -yq --no-install-recommends build-essential \
  && apt-get clean
RUN gem install bundler --version 2.0.1 --force
WORKDIR /app
COPY Gemfile Gemfile.lock ./
RUN bundle install



